This is a single package repository for bashtop package in gentoo portage.

It requires layman to be be installed.

File "pdm-bashtop.xml" from root directory must be integrated as documented in https://wiki.gentoo.org/wiki/Layman, section Adding custom repositories;

Possible corresponding command line: layman -o https://gitlab.com/gentoo-pdm/sys-process/bashtop/pdm-bashtop/pdm-bashtop.xml -f -a pdm-bashtop.

Repository activation: layman -a pdm-bashtop.

Package trial install: emerge -1 bashtop, then emerge bashtop --noreplace to switch to permanent.

Package permanent install: emerge bashtop.
# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Resource monitor that shows usage and stats for processor, memory, disks, network and processes."
HOMEPAGE="https://github.com/aristocratos/bashtop"

if [[ "${PV}" == *9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/aristocratos/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://github.com/aristocratos/${PN}/archive/v${PV}.tar.gz ->${P}.tar.gz "
	KEYWORDS="~amd64 ~x86"
fi

LICENSE=Apache-2.0""
SLOT="0"

IUSE=""

BDEPEND="
	sys-apps/sed
"

RDEPEND="
	>=app-shells/bash-4.4_p23-r1
	sys-apps/coreutils
"

src_prepare() {
	sed -i.bck0 "s#= /usr/local#= /usr#" Makefile || die "Editing path failed"
	sed -i.bck1 "s#share/doc/bashtop#share/doc/${P}#" Makefile || die "Removing doc folder failed"
	default
}
pkg_postinst() {
	optfeature "CPU Temperature support" sys-apps/lm-sensors
	optfeature "Update news and Theme Download feature" net-misc/curl
	optfeature "Disk Stats support" app-admin/sysstat
}
